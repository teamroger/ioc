<?php

namespace Rmb32\IoC\Tests\Unit;

use Rmb32\IoC\Container;
use PHPUnit\Framework\TestCase;
use Rmb32\IoC\Tests\Samples\SimpleThing;
use Rmb32\IoC\Tests\Samples\ComplexThing;

class ContainerTest extends TestCase
{
    protected $container;

    public function setUp() : void
    {
        $this->container = new Container();
    }

    /**
     * @test
     */
    public function contructs_successfully() : void
    {
        $this->assertInstanceOf(Container::class, $this->container);
    }
    
    /**
     * @test
     */
    public function sets_and_gets_an_instance() : void
    {
        $simpleThing = new SimpleThing();
        $this->container->instance(SimpleThing::class, $simpleThing);
        $this->assertSame($simpleThing, $this->container->get(SimpleThing::class));
    }
    
    /**
     * @test
     */
    public function creates_from_available_class_without_binding()
    {
        $object = $this->container->get(SimpleThing::class);

        $this->assertInstanceOf(SimpleThing::class, $object);
    }

    /**
     * @test
     */
    public function binds_and_resoves_a_class_name() : void
    {
        $this->container->bind('ABC', SimpleThing::class);
        
        $object = $this->container->get('ABC');
        $this->assertInstanceOf(SimpleThing::class, $object);
        $this->assertNotSame($object, $this->container->get('ABC'));
    }
    
    /**
     * @test
     */
    public function binds_and_resolves_a_closure() : void
    {
        $this->container->bindClosure('ABC', function () {
            return new SimpleThing();
        });
        
        $object = $this->container->get('ABC');
        $this->assertInstanceOf(SimpleThing::class, $object);
        $this->assertNotSame($object, $this->container->get('ABC'));
    }
    
    /**
     * @test
     */
    public function resolves_class_name_as_a_singleton() : void
    {
        $this->container->singleton('ABC', SimpleThing::class);
        
        $object = $this->container->get('ABC');
        $this->assertInstanceOf(SimpleThing::class, $object);
        $this->assertSame($object, $this->container->get('ABC'));
    }
    
    /**
     * @test
     */
    public function resolves_a_closure_as_a_singleton() : void
    {
        $this->container->singletonClosure('ABC', function () {
            return new SimpleThing();
        });
        
        $object = $this->container->get('ABC');
        $this->assertInstanceOf(SimpleThing::class, $object);
        $this->assertSame($object, $this->container->get('ABC'));
    }

    /**
     * @test
     */
    public function resolves_complex_object() : void
    {
        $object = $this->container->get(ComplexThing::class);

        $this->assertInstanceOf(ComplexThing::class, $object);
        $this->assertInstanceOf(SimpleThing::class, $object->thing);
        $this->assertEquals('Hello World', $object->thing->message);
    }
}
