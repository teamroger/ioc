<?php

declare(strict_types=1);

namespace Rmb32\IoC\Tests\Samples;

class SimpleThing
{
    public $message = 'Hello World';

    public function __construct()
    {
        //
    }
}
