<?php

declare(strict_types=1);

namespace Rmb32\IoC\Tests\Samples;

class ComplexThing
{
    public $thing;

    public function __construct(SimpleThing $thing)
    {
        $this->thing = $thing;
    }
}
