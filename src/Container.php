<?php
/**
 * Class Container | src/Container.php
 *
 * @license proprietary
 */

declare(strict_types=1);

namespace Rmb32\IoC;

use Closure;
use Exception;
use ReflectionClass;

/**
 * An IoC container.
 *
 * @package Rmb32\IoC;
 * @author Roger Barnfather <roger@rogerbarnfather.com>
 */
class Container
{
    /**
     * @var array $instances Stores resolved objects.
     */
    protected $instances = [];

    /**
     * @var array $bindings Stores closures for later resolving to objects.
     */
    protected $bindings  = [];
    
    /**
     * Registers an object instance for retrieval from the container.
     *
     * @param string $key A key for later retrieving the resolved object.
     *     It is highly recommended to use the class's fully qualified name.
     * @param object $instance The object instance.
     * @return void
     */
    public function instance(string $key, object $instance) : void
    {
        $this->instances[$key] = $instance;
    }
    
    /**
     * Registers a class from which object instances can be created
     * and returned later by the container.
     *
     * @param string $key A key for later retrieving the resolved object.
     *     It is highly recommended to use the class's fully qualified name.
     * @param string $class The class name.
     * @return void
     */
    public function bind(string $key, string $class) : void
    {
        $this->bindings[$key] = function () use ($class) {
            return $this->autoResolve($class);
        };
    }
    
    /**
     * Registers a closure for creating object instances that get retured
     * later by the container.
     *
     * @param string $key A key for later retrieving the resolved object.
     *     It is highly recommended to use the class's fully qualified name.
     * @param \Closure $closure The closure to resolve objects from.
     * @return void
     */
    public function bindClosure(string $key, Closure $closure) : void
    {
        $this->bindings[$key] = $closure;
    }
    
    /**
     * Registers a class from which a single object can be created and
     * retrieved from container as many times as needed.
     *
     * @param string $key A key for later retrieving the resolved object.
     *     It is highly recommended to use the class's fully qualified name.
     * @param string $class The class name.
     * @return void
     */
    public function singleton(string $key, string $class) : void
    {
        $this->bindings[$key] = function () use ($key, $class) {
            $this->instances[$key] = $this->autoResolve($class);
            
            return $this->instances[$key];
        };
    }
    
    /**
     * Registers a closure from which a single object can be created and
     * retrieved from the container as many times as needed.
     *
     * @param string $key A key for later retrieving the resolved object.
     *     It is highly recommended to use the class's fully qualified name.
     * @param \Closure The closure to resolve the instance from.
     * @return void
     */
    public function singletonClosure(string $key, Closure $closure) : void
    {
        $this->bindings[$key] = function () use ($key, $closure) {
            $this->instances[$key] = $closure($this);
            
            return $this->instances[$key];
        };
    }
    
    /**
     * Retrieves an object from the container.
     *
     * @param string $key The key to retrieve the object.
     * @return object $instance The object instance.
     * @throws \Exception If the key hasn't been set.
     */
    public function get(string $key) : object
    {
        if (array_key_exists($key, $this->instances)) {
            return $this->instances[$key];
        }
        
        if (array_key_exists($key, $this->bindings)) {
            return $this->bindings[$key]($this);
        }

        return $this->autoResolve($key);
    }
    
    /**
     * Resolves the given class name to an object.
     *
     * Depending on the class to be intantiated, this method can
     * recursively resolve dependencies to pass into the constructor.
     *
     * @param string $class The class to resolve.
     * @return object The resolved object instance.
     * @throws \Exception If there were issues resolving the class.
     */
    protected function autoResolve(string $class) : object
    {
        if (!class_exists($class)) {
            throw new Exception("$class not found");
        }
        
        $reflectionClass = new ReflectionClass($class);
        
        if (!$reflectionClass->isInstantiable()) {
            throw new Exception("$class is not instantiable");
        }
        
        $constructor = $reflectionClass->getConstructor();
        
        if (null === $constructor) {
            return $reflectionClass->newInstance();
        }
        
        $parameters = $constructor->getParameters();
        
        $dependencies = [];
        
        foreach ($parameters as $parameter) {
            $typeHint = $parameter->getClass()->getName();
            
            $dependencies[$typeHint] = $this->get($typeHint);
        }
        
        return $reflectionClass->newInstanceArgs($dependencies);
    }
}
